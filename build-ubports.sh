#!/bin/bash

set -Eeou pipefail

# Download latest SDL
URL_LATEST=https://gitlab.com/abmyii/sdl/-/jobs/artifacts/ubports/download?job=xenial_${ARCH}_deb

wget ${URL_LATEST} -O build.zip
unzip build.zip
dpkg -i --force-overwrite build/libsdl2_*.deb
dpkg -i --force-overwrite build/libsdl2-dev_*.deb

rm /usr/lib/${ARCH_TRIPLET}/libSDL2-2.0.so.0.4.0
ls -alh /usr/lib/${ARCH_TRIPLET}/libSDL2-2.0.so*


cd "${ROOT}"
mkdir cmake-build && cd cmake-build
#./configure CFLAGS='-D__UBPORTS__' CXXFLAGS='-D__UBPORTS__' --prefix="/" --host="${ARCH_TRIPLET}" --binary-dir="lib/${ARCH_TRIPLET}/bin" --install-dir="${INSTALL_DIR}" --with-freetype --with-xdg-basedir --personal-dir=.openttd.abmyii --shared-dir=/.local/share/openttd.abmyii
cmake .. -DCFLAGS='-D__UBPORTS__' -DCXXFLAGS='-D__UBPORTS__' -Dprefix="/" -Dhost="${ARCH_TRIPLET}" -Dbinary-dir="lib/${ARCH_TRIPLET}/bin" -Dinstall-dir="${INSTALL_DIR}" -Dfreetype=ON -Dxdg-basedir=ON -Dpersonal-dir=.openttd.abmyii -Dshared-dir=/.local/share/openttd.abmyii
make -j$(( ( $(getconf _NPROCESSORS_ONLN) + 1) / 2 ))
make DESTDIR="${INSTALL_DIR}" install

# Download OpenGFX to baseset
cd "${ROOT}"/bin/baseset
curl -L https://cdn.openttd.org/opengfx-releases/7.1/opengfx-7.1-all.zip > opengfx-all.zip
unzip -o opengfx-all.zip
rm -f opengfx-all.zip
